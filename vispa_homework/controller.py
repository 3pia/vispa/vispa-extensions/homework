# -*- coding: utf-8 -*-

import cherrypy
import json
from vispa.models.role import Permission
from vispa.models.project import Project
from vispa.models.project import ProjectItem
from vispa import AjaxException
from vispa.controller import AbstractController
import skeletons
import tools
import os
import uuid


class HomeworkController(AbstractController):

    COURSE_PATH = "/home/test/course_dir"

    def __init__(self, extension):
        super(HomeworkController, self).__init__()
        self.extension = extension
        self.um = self.extension.server.controller.ajax.um

    def setup(self, project_name, permission_name):
        session = cherrypy.request.db
        user = cherrypy.request.user
        permission = Permission.get(session, permission_name)
        project = Project.get(session, project_name)
        return user, project, permission

    def getrpc(self):
        rpc = self.get("proxy", "HomeworkRpc", self.get("combined_id"))
        if rpc is not None:
            return rpc
        else:
            raise AjaxException("Could connect to worker")

    def get_course_meta(self, project_name):
        courses = self.get_item(project_name, "homework.meta",
                                "homework.read_course")
        if len(courses) == 1:
            return courses[0]
        elif len(courses) > 1:
            print("There should only be one course meta item but there were %d"
                  % len(courses))
        return None

    def get_item(self, project_name, itemtype, permission,
                 extract_content=True):
        user, project, permission = self.setup(project_name, permission)
        if user.has_permission(permission, project):
            if extract_content:
                return [json.loads(item.content) for item in
                        project.get_items(itemtype=itemtype)]
            else:
                return project.get_items(itemtype=itemtype)
        else:
            raise AjaxException(403)

    def get_permissions(self, course_name, shortcuts=True):
        session = cherrypy.request.db
        user = cherrypy.request.user
        project = Project.get(session, course_name)
        permissions = skeletons.permission_dict()

        for perm_name in permissions.keys():
            full_perm_name = "homework.%s" % perm_name
            permission = Permission.get(session, full_perm_name)
            if user.has_permission(permission, project):
                permissions[perm_name] = True

        if shortcuts:
            permissions["admin"] = (permissions["create_course"] and
                                    permissions["create_assignment"])
            permissions["tutor"] = permissions["submit_correction"]
            permissions["student"] = (not permissions["submit_correction"] and
                                      permissions["submit_solution"])

        return permissions

    def check_permissions(self, project_name, *permission_names):
        session = cherrypy.request.db
        user = cherrypy.request.user
        project = Project.get(session, project_name)

        for perm_name in permission_names:
            permission = Permission.get(session, perm_name)
            if not user.has_permission(permission, project):
                raise AjaxException(403)

    def get_all_permissions(self):
        session = cherrypy.request.db
        user = cherrypy.request.user
        project_names = [project["name"] for project in
                         self.um.user_get_projects()]
        permissions = {}

        for project_name in project_names:
            project_permissions = skeletons.permission_dict()

            for permission_name in project_permissions.keys():
                permission = Permission.get(session,
                                            "homework." + permission_name)
                project = Project.get(session, project_name)
                if user.has_permission(permission, project):
                    project_permissions[permission_name.replace("homework.",
                                                                "")] = True
            permissions[project_name] = project_permissions
        return permissions

    @cherrypy.expose
    def get_user_info(self):
        rpc = self.getrpc()

        user = {}
        user["home"] = rpc.get_home_env()
        user["perm"] = self.get_all_permissions()
        return user

    @cherrypy.expose
    def update_course(self, course):
        new_course = json.loads(course)
        name = new_course["name"]
        self.check_permissions(name, "homework.create_course")
        course = self.get_item(name, "homework.meta",
                               "homework.read_course", extract_content=False)

        if len(course) != 1:
            raise AjaxException("Could not find the referenced course")
        else:
            course = course[0]

        data = tools.safe_merge(json.loads(course.content), new_course)
        course.set_content(unicode(json.dumps(data)))
        print(data["maxGroupSize"])
        return course.content

    @cherrypy.expose
    def create_course(self, course):
        course = json.loads(course)
        course["isItem"] = True
        course["id"] = str(uuid.uuid4())
        course["path"] = os.path.join(self.COURSE_PATH, course["id"])

        rpc = self.getrpc()
        try:
            rpc.create_course_dir(course["path"])
        except IOError as e:
            raise AjaxException(e.message)

        user, project, permission = self.setup(course["name"],
                                               "homework.create_course")

        if self.get_course_meta(course["name"]):
            print("A course with this name already exists.")
        elif user.has_permission(permission, project):
            course = unicode(json.dumps(tools.merge_course(course)))
            ProjectItem.create(cherrypy.request.db, project,
                               u"homework.meta", course)
            return course
        else:
            raise AjaxException(403)

    @cherrypy.expose
    def get_courses(self):
        project_names = [project["name"]
                         for project in self.um.user_get_projects()
                         if project["name"] != "VISPA"]

        courses = []

        for project in project_names:
            meta = self.get_course_meta(project)
            if not meta:
                meta = skeletons.course(project)
            assignments = self.get_item(project, "homework.assignment",
                                        "homework.read_assignment")
            meta["assignments"] = assignments
            meta["perm"] = self.get_permissions(project)
            courses.append(meta)

        return sorted(courses, key=lambda course: course["name"])

    @cherrypy.expose
    def create_assignment(self, course_name, assignment):
        assignment = json.loads(assignment)
        unavailable_names = [assign["name"] for assign in
                             self.get_item(course_name, "homework.assignment",
                                           "homework.read_assignment")]

        if (not tools.assignment_check(assignment) or
           assignment["name"] in unavailable_names):
            raise AjaxException(400)

        course_meta = self.get_course_meta(course_name)
        assignment_dir = os.path.join(course_meta["path"], assignment["name"])
        files_dir = os.path.join(assignment_dir, 'files')
        assignment["path"] = assignment_dir

        rpc = self.getrpc()
        try:
            rpc.copy(files_dir, assignment["files"])

        except IOError as e:
            raise AjaxException(e.message)

        old_files = assignment["files"]
        assignment["files"] = []

        for filepath in old_files:
            _, fname = os.path.split(filepath)
            assignment["files"].append(os.path.join(files_dir, fname))

        new_assignment = unicode(json.dumps(tools.merge_assignment(assignment)))

        user, project, permission = self.setup(course_name,
                                               "homework.create_assignment")

        if user.has_permission(permission, project):
            ProjectItem.create(cherrypy.request.db, project,
                               u"homework.assignment", new_assignment)
            return json.loads(new_assignment)
        else:
            raise AjaxException(403)

    @cherrypy.expose
    def delete_assignment(self, course_name, assignment_name):
        user, project, permission = self.setup(course_name,
                                               "homework.create_assignment")

        self.check_permissions(course_name, "homework.create_assignment")

        assignments = self.get_item(project, "homework.assignment",
                                    "homework.read_assignment",
                                    extract_content=False)

        contents = [json.loads(a.content) for a in assignments]

        for i, assignment in enumerate(assignments):
            if contents[i]["name"] == assignment_name:
                assignment.delete(cherrypy.request.db)
                rpc = self.getrpc()
                rpc.remove_dir(contents[i]["path"])
                return

        raise AjaxException("Cannot find assignment named '%s' in course '%s'" %
                            (assignment_name, course_name))

    @cherrypy.expose
    def copy_files(self, dest, files, folder=None):
        if folder is not None:
            dest = os.path.join(dest, folder)

        rpc = self.getrpc()
        files = json.loads(files)

        try:
            rpc.copy(dest, files)
        except IOError as e:
            raise AjaxException(e.message)
