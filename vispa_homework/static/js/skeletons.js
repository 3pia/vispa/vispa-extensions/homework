define(function(){

  function studentInfo(){
    return {
      firstName: "",
      lastName: "",
      matrNr: "",
      accountName: "",
    };
  }

  function assignmentSkeleton(){
    return {
      name: "",
      deadline: (new Date()).getTime(),
      files: [],
      students: [studentInfo()],
      submittedFiles: [],
      correctedFiles: [],
      points: 0,
      maxPoints: 0,
    };
  }

  return {
    "assignment": assignmentSkeleton,
    "studentInfo": studentInfo,
  };
});
