define(["jquery",
    "vispa/views/main",
    "vue/vue",
    "vue/vue-strap",
    "./prefs",
    "./skeletons",
    "./main_template",
  ], function(
    $,
    MainView,
    Vue,
    VueStrap,
    Prefs,
    Skeletons,
    HomeworkViewContent
  ){

  var HomeworkView = MainView._extend({

    init: function init(args){
      init._super.apply(this, arguments);

      this.state.setup({
        path: undefined,
        previewPath: undefined,
        command: undefined,
        writeable: true,
      }, args);
    },

    applyPreferences: function applyPreferences(){
      applyPreferences._super.call(this);
      return this;
    },

    render: function(node){
      //this.setLoading(true);

      this.vue.content = new HomeworkViewContent({
        el: node[0],
        view: this,
        data: {
          currentAssignment: null,
          currentCourse: null,
          contentView: null,
          courses: [],
          perm: {},
          skeletons: Skeletons,
          userInfo: {},
        },
      });

      this.applyPreferences();
      this.icon = "fa-envelope";
    },
  }, {
    iconClass: "fa-envelope",
    label: "Homework",
    name: "HomeworkView",
    menuPosition: 30,
    preferences: {
      items: Prefs.preferences,
    },
    menu: Prefs.menu,
  });

  return HomeworkView;
});
