define(["vispa/extension", "./view", "css!../css/style"],
				function(Extension, HomeworkView){

	var HomeworkExtension = Extension._extend({

		init: function init(){
			init._super.call(this, "homework", "Homework");

		  this.mainMenuAdd([
      	this.addView(HomeworkView),
      ]);
		},
	});

	return HomeworkExtension;
});
