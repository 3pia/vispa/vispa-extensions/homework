define([
    "vue/vue",
    "vue/vue-strap",
    "./components/student_view",
    "./components/new_assignment",
    "./components/edit_course",
    "./components/admin_view",
    "text!../html/main_template.html",
  ], function(
    Vue,
    VueStrap,
    StudentView,
    NewAssignment,
    EditCourse,
    AdminView,
    mainTemplate
  ){

  var HomeworkViewContent = Vue.extend({

    template: mainTemplate,

    components: {
      "student-view": StudentView,
      "new-assignment" : NewAssignment,
      "edit-course": EditCourse,
      "admin-view" : AdminView,
    },

    methods: {

      showContent: function(content, course, assignment){
        this.currentCourse = course;
        this.currentAssignment = assignment;
        this.contentView = content;
      },

      updateCourses: function(){
        this.$options.view.setLoading(true);

        this.$options.view.GET("get_courses", function(err, data){
          if (err) return;
          this.courses = data;
          this.$options.view.setLoading(false);
        }.bind(this));
      },

      updateUserInfo: function(){
        this.$options.view.GET("get_user_info", function(err, data){
          if (err) return;
          this.user = data;
        }.bind(this));
      },

    },

    ready: function(){
      this.updateCourses();
      this.updateUserInfo();
    },
  });

  return HomeworkViewContent;
});
