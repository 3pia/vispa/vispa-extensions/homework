define([
    "vue/vue",
    "text!../../html/admin_view.html",
  ], function(
    Vue,
    adminTemplate
  ){

  var AdminView = Vue.extend({
    template: adminTemplate,

    props: {
      course: Object,
      assignment: Object,
      view: Object,
      skeletons: Object,
    },

    data: function(){
      return {confirm: false};
    },

    filters: {
      date: function(assignment){
        return (new Date(assignment.deadline)).toISOString().slice(0, 10);
      },
    },

    methods: {
      deleteAssignment: function(){
        this.view.GET("delete_assignment", {
            course_name: this.course.name,
            assignment_name: this.assignment.name,
          }, function(err){
            if (err) {
              this.view.alert(err);
            } else {
              this.$parent.updateCourses();
              this.$parent.showContent();
            }
        }.bind(this));
      },
    },
  });

  return AdminView;
});
