define([
    "vue/vue",
    "text!../../html/student_view.html",
  ], function(
    Vue,
    studentTemplate
  ){

  var StudentView = Vue.extend({
    template: studentTemplate,

    props: {
      course: Object,
      assignment: Object,
      view: Object,
      skeletons: Object,
    },

    computed: {
      expired: function(){
        return Date.now() > this.assignment.deadline;
      },

      deadlineDate: function(){
        return new Date(this.assignment.deadline);
      },

      submitted: function(){
        return this.assignment.submittedFiles.length > 0;
      },

      corrected: function(){
        return this.assignment.correctedFiles.length > 0;
      },

      gradeColor: function(){
        var scale = this.assignment.points / this.assignment.maxPoints;
        if (scale < 0.25){
          return "#FF0000";
        } else if (scale < 0.5){
          return "#FF6C00";
        } else if (scale < 0.75){
          return "#FFD700";
        } else {
          return "#5CB85C";
        }
      },

      fileListLength: function(){
        var maxIndex = Math.max(
          this.assignment.files.length,
          this.assignment.submittedFiles.length,
          this.assignment.correctedFiles.length
        );
        var indexList = [];
        for (var i=0; i < maxIndex; i++){
          indexList.push(i);
        }
        return indexList;
      },
    },

    methods: {

      removeInfo: function(){
        if (this.assignment.students.length > 1){
          this.assignment.students.pop();
        }
      },

      addInfo: function(){
        if (this.assignment.students.length < this.course.maxGroupSize){
          this.assignment.students.push(this.skeletons.studentInfo());
        }
      },

      getFiles: function(){
        // this would be nice here but it can't be canceled if the file selector
        // is closed
        // this.$parent.$options.view.setLoading(true);

        var args = {
          // path: path,
          multimode: false,
          foldermode: true,
          filemode: false,
          callback: function(path) {
            path = path.replace("$HOME", this.$parent.user.home);

            this.view.GET("copy_files", {
                dest: path,
                files: JSON.stringify(this.assignment.files),
                folder: this.assignment.name
              }, function(err){
                this.$parent.$options.view.setLoading(false);
                if (err) {
                  this.view.alert(err);
                }
                else {
                  this.view.alert("The files were copied successfully to " + path);
                }
            }.bind(this));

          }.bind(this),
          sort: "name",
          reverse: false
        };

        this.view.spawnInstance("file", "FileSelector", args);

      },

      fileTableEntry: function(columnName, index){
        if (this.assignment[columnName] === undefined ||
            this.assignment[columnName].length <= index){
          return undefined;
        } else {
          return this.assignment[columnName][index].split("/").slice(-1)[0];
        }
      },

      submit: function(){
      },

      spawnFileBrowser: function(){
        this.view.spawnInstance("file", "FileBrowser", {});
      },
    },

    filters: {
      date: function(assignment){
        if (assignment && assignment.deadline){
          return (new Date(assignment.deadline)).toISOString().slice(0, 10);
        } else {
          return "?";
        }
      },

      deltaDays: function(assignment){
        console.log(new Date());
        console.log(new Date(assignment.deadline));
        var delta = Math.abs((new Date(assignment.deadline)) - new Date());
        var days = delta / (1000*60*60*24);
        var hours = Math.floor((days % 1) * 24);
        days = Math.floor(days);

        var deltaString = "";
        if (days >= 1){
          deltaString += days + " days ";
        }
        return deltaString + hours + " hours remaining";
      }
    }
  });

  return StudentView;
});
