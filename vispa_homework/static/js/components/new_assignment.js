define([
    "vue/vue",
    "vue/vue-strap",
    "text!../../html/new_assignment.html",
  ], function(
    Vue,
    VueStrap,
    newAssignTemplate
  ){

  var NewAssignment = Vue.extend({
    template: newAssignTemplate,

    components: {
      // TODO: this is deprecated but the datepicker question is still open
      "dateepicker": VueStrap.datepicker,
    },

    props: {
      course: Object,
      assignment: Object,
      view: Object,
      skeletons: Object,
    },

    computed: {
      deadline: {
        get: function(){
          var dateObj = new Date(this.assignment.deadline);
          return dateObj.toISOString().substring(0, 10);
        },

        set: function(datestring){
          this.assignment.deadline = (new Date(datestring)).getTime();
        },
      },
    },

    methods: {
      fileSelector: function(){
        var args = {
          // path: path,
          multimode: true,
          foldermode: false,
          callback: function(path) {
            for (let i = 0; i < path.length; i++){
              path[i] = path[i].replace("$HOME", this.$parent.user.home);
            }
            this.assignment.files = this.assignment.files.concat(path);
            console.log(this.assignment.files);
          }.bind(this),
          sort: "name",
          reverse: false
        };

        this.view.spawnInstance("file", "FileSelector", args);
      },

      create: function(){

        if (!this.assignment.name){
          this.view.alert("Please specify a name for the assignment.");
          return;
        }

        for (let i = 0; i < this.course.assignments.length; i++){
          if (this.course.assignments[i].name === this.assignment.name){
            this.view.alert("This name is already in use. Please choose another one.");
            return;
          }
        }

        this.$parent.$options.view.setLoading(true);


        this.view.POST("create_assignment", {
            course_name: this.course.name,
            assignment: JSON.stringify(this.assignment),
          }, function(err, data){
            this.$parent.$options.view.setLoading(false);
            if (err) return;
            console.log("handling");
            this.$parent.updateCourses();
            this.$parent.showContent("admin-view", this.course, data);
        }.bind(this));

      },
    },

  });

  return NewAssignment;
});
