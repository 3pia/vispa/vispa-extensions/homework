define([
    "vue/vue",
    "vue/vue-strap",
    "text!../../html/edit_course.html",
  ], function(
    Vue,
    VueStrap,
    newCourseTemplate
  ){

  var EditCourse = Vue.extend({
    template: newCourseTemplate,

    props: {
      course: Object,
      assignment: Object,
      view: Object,
      skeletons: Object,
    },

    methods: {
      submit: function(){
        if (this.course.maxGroupSize <= 0){
          this.view.alert("Invalid Group Size");
          return;
        }

        var url = this.course.isItem? "update_course" : "create_course";

        this.view.POST(url, {course: JSON.stringify(this.course)}, function(err, data){
          if (err) return;
          if (this.course.isItem) {
            this.$parent.$options.view.alert("Course successfully edited");
          }
          Vue.util.extend(this.course, JSON.parse(data));
        }.bind(this));
      },

      browse: function(){
        var args = {
          multimode: false,
          foldermode: true,
          callback: function(path) {
            this.course.path = path.replace("$HOME", this.$parent.user.home);
          }.bind(this),
          sort: "name",
          reverse: false
        };

        this.view.spawnInstance("file", "FileSelector", args);
      },

    }
  });

  return EditCourse;
});
