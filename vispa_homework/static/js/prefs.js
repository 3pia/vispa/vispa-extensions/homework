define(function(){
	/* Example
	var metaCtrl = function(key){
		return "mac:meta+" + key + " ctrl+" + key;
	};
	*/

	var defaultPreferences = {
		/*
		matrnr: {
			level: 0,
			label: "Matriculation Number",
			descr: "Matriculation/Registration number for homework submission.",
			type: "string", // maybe int would be better
			value: "",
		},
    */
	};

	var menu = {
    save: {
      label: "Refresh",
      iconClass: "fa-refresh",
      callback: function() {
        this.$root.instance.vue.content.updateCourses();
        this.$root.instance.vue.content.showContent();
      },
      unique: true,
      priority: 1
    }
	};

	return {
		preferences: defaultPreferences,
		menu: menu,
	};

});
