# -*- coding: utf-8 -*-


def course(name=""):
    return {
        "name": name,
        "path": "",
        "id": "",
        "isItem": False,
        "maxGroupSize": 1,
        "assignments": [],
    }


def assignment(initial_account_name=None):
    return {
        "name": "",
        "deadline": 0,
        "path": "",
        "files": [],
        "students": [empty_student(initial_account_name)],
        "submittedFiles": [],
        "correctedFiles": [],
        "points": 0,
        "maxPoints": 0,
    }


def empty_student(account_name=""):
    return {
        "firstName": "",
        "lastName": "",
        "matrNr": "",
        "accountName": account_name if account_name is not None else "",
    }


def permission_dict():
    return {
        "read_course": False,
        "read_assignment": False,
        "create_course": False,
        "create_assignment": False,
        "submit_solution": False,
        "submit_correction": False,
    }
