# -*- coding: utf-8 -*-

from vispa.server import AbstractExtension
from controller import HomeworkController


class HomeworkExtension(AbstractExtension):

    def name(self):
        return "homework"

    def dependencies(self):
        return []

    def setup(self):
        self.add_controller(HomeworkController(self))
        self.add_workspace_directoy()
