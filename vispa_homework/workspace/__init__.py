# -*- coding: utf-8 -*-

import logging
import os
import shutil

logger = logging.getLogger(__name__)


class InvalidDirectoryError(Exception):
    pass


class HomeworkRpc:
    def __init__(self):
        logger.debug("HomeworkRpc instance created")

    def copy(self, destination, fnames):
        try:
            if len(fnames) == 1 and os.path.isdir(fnames[0]):
                source_dir = fnames[0]
                fnames = []
                for _, _, f in os.walk(source_dir):
                    fnames += f

            if not os.path.isdir(destination):
                os.makedirs(destination)

            for fname in fnames:
                destination = os.path.expanduser(destination)
                fname = os.path.expanduser(fname)
                assert os.path.isfile(fname), fname + " is not a file"
                shutil.copy(fname, destination)
                new_fname = os.path.join(destination, os.path.basename(fname))
                assert os.path.isfile(new_fname), "New file does not exist"

        except AssertionError as e:
            raise IOError(e.message)

    def remove_dir(self, dir):
        # TODO: remove this safeguard for production
        assert dir.startswith("/home/erik/vispatest")
        if os.path.isdir(dir):
            shutil.rmtree(dir)

    def get_home_env(self):
        return os.environ["HOME"]

    def create_course_dir(self, path):
        if os.path.isdir(path):
            if len(os.listdir(path)) != 0:
                raise IOError("Directory should not exist or be empty")
        else:
            os.makedirs(path)

    def create_assignment_dirs(self, course_name, assignment_name):
        pass
