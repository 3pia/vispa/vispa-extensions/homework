import skeletons


def safe_merge(master, dict):
    for key in master.keys():
        if key in dict.keys():
            master[key] = dict[key]
    return master


def list_check(str_list, dtype=unicode, max_str_length=20):
    try:
        assert type(str_list) == list
        for el in str_list:
            assert type(el) == dtype
            if dtype == str:
                assert len(el) < max_str_length
        return True
    except:
        return False


def assignment_check(assignment, max_str_length=20):
    try:
        assert type(assignment) == dict
        assert type(assignment["name"]) == unicode
        assert len(assignment["name"]) < max_str_length
        assert type(assignment["deadline"]) == int
        assert type(assignment["points"]) == int
        assert type(assignment["maxPoints"]) == int
        assert list_check(assignment["files"], unicode, max_str_length)
        assert list_check(assignment["submittedFiles"], unicode, max_str_length)
        assert list_check(assignment["correctedFiles"], unicode, max_str_length)
        assert list_check(assignment["students"], dict)
        return True
    except AssertionError as err:
        return False


def merge_assignment(assignment_dict):
    assignment_check(assignment_dict)
    master = skeletons.assignment()
    return safe_merge(master, assignment_dict)


def merge_course(course_dict):
    master = skeletons.course()
    return safe_merge(master, course_dict)
