#!/usr/bin/env python

# -*- coding: utf-8 -*-

from setuptools import setup, find_packages

setup(

    name             = "vispa_homework_extension",

    version          = "0.1.0",

    description      = "VISPA Homework Extension - Helps students to submit their homework.",

    author           = "VISPA Project",

    author_email     = "vispa@lists.rwth-aachen.de",

    url              = "http://vispa.physik.rwth-aachen.de/",

    license          = "GNU GPL v2",

    packages         = find_packages(),

    package_dir      = {"vispa_homework": "vispa_homework"},

    package_data     = {"vispa_homework": [

        "workspace/*",

        "static/css/*",

        "static/js/*",

        "static/html/*",

        "static/img/*"

    ]},

    # install_requires = ["vispa"],

)
